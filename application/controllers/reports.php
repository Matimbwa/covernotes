<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH."/third_party/PHPExcel.php";

class Reports extends CI_Controller {

function __construct()
		{
			parent::__construct();
			$this->load->model('model');
             $this->load->library('excel');
             $real = realpath('C:/wamp/www/covernotes');
		}

	
	public function reports()
	{
		$data['contents'] = 'reports/reports_all';
		$data['beneficiaries'] = $this->model->getBeneficiaries();
		$data['covernotestypes'] = $this->model->getCovernotestypes();
		$data['covernotes'] = $this->model->getReports();
		$this->load->view('layout/master',$data);
	}
	

    public function allCovernotes()
    {
                $this->excel->setActiveSheetIndex(0);
                //name the worksheet
                $this->excel->getActiveSheet()->setTitle('cover_notes');
                //set cell A1 content with some text
                $this->excel->getActiveSheet()->setCellValue('A1', 'List of cover notes');
                $this->excel->getActiveSheet()->setCellValue('A3', 'S.No.');
                $this->excel->getActiveSheet()->setCellValue('B3', 'Cover type');
                $this->excel->getActiveSheet()->setCellValue('C3', 'Insuarer');
                $this->excel->getActiveSheet()->setCellValue('D3', 'Cover #');
                $this->excel->getActiveSheet()->setCellValue('E3', 'First Name');
                $this->excel->getActiveSheet()->setCellValue('F3', 'Last Name');
                $this->excel->getActiveSheet()->setCellValue('G3', 'Mobile');
                $this->excel->getActiveSheet()->setCellValue('H3', 'Email');
                $this->excel->getActiveSheet()->setCellValue('I3', 'Vehicle name');
                $this->excel->getActiveSheet()->setCellValue('J3', 'Vehicle Reg #');
                $this->excel->getActiveSheet()->setCellValue('K3', 'First date');
                $this->excel->getActiveSheet()->setCellValue('L3', 'Expire date');
                //merge cell A1 until L1
                $this->excel->getActiveSheet()->mergeCells('A1:L1');
                   //merge cell A2 until L2
                $this->excel->getActiveSheet()->mergeCells('A2:L2');

                //set aligment to LEFT for that merged cell (A1 to L1)
                $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                //make the font become bold
                 $this->excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
                 $this->excel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
                 $this->excel->getActiveSheet()->getStyle('C3')->getFont()->setBold(true);
                 $this->excel->getActiveSheet()->getStyle('D3')->getFont()->setBold(true);
                 $this->excel->getActiveSheet()->getStyle('E3')->getFont()->setBold(true);
                 $this->excel->getActiveSheet()->getStyle('F3')->getFont()->setBold(true);
                 $this->excel->getActiveSheet()->getStyle('G3')->getFont()->setBold(true);
                 $this->excel->getActiveSheet()->getStyle('H3')->getFont()->setBold(true);
                 $this->excel->getActiveSheet()->getStyle('I3')->getFont()->setBold(true);
                 $this->excel->getActiveSheet()->getStyle('J3')->getFont()->setBold(true);
                 $this->excel->getActiveSheet()->getStyle('K3')->getFont()->setBold(true);
                 $this->excel->getActiveSheet()->getStyle('L3')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
                $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#000080');
       for($col = ord('A'); $col <= ord('L'); $col++){
                //set column dimension
                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
                 //change the font size
                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                 
                $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        }
                //retrive cover notes table data
                 $this->db->select('id,cover_type,beneficiary,cover_no,f_name,l_name,mobile,email,v_name,v_no,f_date,e_date');
                
                $rs = $this->db->get('cover_notes_view');
                // $rs = $this->db->get('cover_notes_view');
                $exceldata="";
                foreach ($rs->result_array() as $row){
                $exceldata[] = $row;

              
        }
                //Fill data 
                $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A4');
                 
                $this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $this->excel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                 
                $filename= mt_rand(1,100000).'.xls'; //just some random filename
                 //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
 
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');
                // save to folder
                $objWriter->save('export/'.$filename);
             exit; //done.. exiting!   
    }

	function repot()
    {
        $query = $this->db->get('cover_notes');
        if(!$query)
            return false;
        // Starting the PHPExcel library




        $this->load->library('PHPExcel');
        $this->load->library('PHPExcel/IOFactory');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);
        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }
        // Fetching the table data
        $row = 2;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
            $row++;
        }
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Products_'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save("nameoffile.xls");
        $objWriter->save('php://output');
    }
   }