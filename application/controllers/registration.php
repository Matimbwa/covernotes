<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

	function __construct()
		{
			parent::__construct();
			$this->load->model('model');

		}



	public function coverNotes()
	{
		// validation
		$this->form_validation->set_rules('cover_no','Cover number','trim|required');
		$this->form_validation->set_rules('f_name','Customer first name','trim|required');
		$this->form_validation->set_rules('l_name','Customer last name','trim|required');
		$this->form_validation->set_rules('mobile_no','Mobile number','trim|required');
		$this->form_validation->set_rules('email','Email','trim|valid_email|required');
		$this->form_validation->set_rules('vehicle_name','vehicle name','trim|required');
		$this->form_validation->set_rules('vehicle_no','vehicle number','trim|required');
		// $this->form_validation->set_rules('covertype_id','Cover type','trim|required|callback_select_validate');
		// $this->form_validation->set_rules('beneficiary_id', 'beneficiary', 'required|callback_select_validate');
		// $this->form_validation->set_message('beneficiary_id', 'You need to select something other than the default');
		// |callback_select_validate'

		// $this->form_validation->set_rules('first_date','First date','trim|required');
		// $this->form_validation->set_rules('expire_date','expire date','trim|required');



		if ($this->form_validation->run()==FALSE)
		 {

		$data['beneficiaries'] = $this->model->getBeneficiaries();
		$data['covernotestypes'] = $this->model->getCovernotestypes();
		$data['contents'] = 'registration/cover_notes';
		$this->load->view('layout/master',$data);
		}
		else
		{
				$coverno=$this->input->post('cover_no');
				$covertype_id=$this->input->post('covertype_id');
				$beneficiary_id=$this->input->post('beneficiary_id');

				$fname=ucfirst($this->input->post('f_name'));	
				$lname=ucfirst($this->input->post('l_name'));	
				$mobile=$this->input->post('mobile_no');	
				$email=$this->input->post('email');

				$vehicle_name=ucfirst($this->input->post('vehicle_name'));	
				$vehicle_no=$this->input->post('vehicle_no');

			
				$data = array(
				'cover_notes_type_id' => $covertype_id,
				'beneficiary_id' =>$beneficiary_id,
				'cover_no' =>$coverno,
				'customer_fname' =>$fname,
				'customer_lname' =>$lname,
				'mobile_no' =>$mobile,
				'email' =>$email,
				'vehicle_name' =>$vehicle_name,
				'vehicle_no' =>$vehicle_no,
				'first_date' =>2015-10-10,
				'expire_date' =>2016-10-09
				);

				var_dump($data);
				
				$this->db->insert('cover_notes',$data);
				$this->session->set_flashdata('message', 'Hello! Cover notes details have been added successifully');
				// // $this->load->model('MyModel',$data);
				// // $this->model->addBeneficiary($data);

				// redirect('covernotes');
				$this->session->set_flashdata('mail',$email);	
				redirect('mailcustomer');

		}
	}

	public function coverNotestypes()
	{	
		// validation
			$this->form_validation->set_rules('covernote','Covernotes name','trim|required');

			if ($this->form_validation->run()==FALSE)
		 	{
		 	
			$data['title']='';
			$data['contents'] = 'registration/cover_notestypes';
			$this->load->view('layout/master',$data);

			}
			else{
				$covernotestype=ucfirst($this->input->post('covernote'));	
						
				$data = array(
				'type' => $covernotestype
				);
				
				$this->db->insert('cover_notes_types',$data);
				$this->session->set_flashdata('message', 'A covernote type has been added successifully');
				// $this->load->model('MyModel',$data);
				// $this->model->addBeneficiary($data);

				redirect('covernotes_types');

			}
	}

	public function beneficiaries()
	{	
		// validation
			$this->form_validation->set_rules('beneficiary','Beneficiary name','trim|required');
			$this->form_validation->set_rules('email','Email','trim|valid_email|required');
			$this->form_validation->set_rules('mobile','Mobile number','trim|required');

			if ($this->form_validation->run()==FALSE)
		 	{
		 	
			$data['title']='';
			$data['contents'] = 'registration/beneficiaries';
			$this->load->view('layout/master',$data);

			}
			else{
				$beneficiary=ucfirst($this->input->post('beneficiary'));	
				$email=$this->input->post('email');
				$mobile=$this->input->post('mobile');
						
				$data = array(
				'name' => $beneficiary,
				'email' =>$email,
				'mobile_no' =>$mobile
				);
				
				$this->db->insert('beneficiaries',$data);
				$this->session->set_flashdata('message', 'Beneficiary details have been added successifully');
				// $this->load->model('MyModel',$data);
				// $this->model->addBeneficiary($data);

				redirect('beneficiaries');

			}
	}
	public function policies()
	{	
		
		 	
			$data['title']='';
			$data['contents'] = 'registration/policies';
			$this->load->view('layout/master',$data);

			
	}
}
