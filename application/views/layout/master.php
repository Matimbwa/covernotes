<!DOCTYPE html>
<html>
  <head>
    <title>Cover notes syst</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- styles -->
    <link href="<?php echo base_url('assets/css/styles.css" rel="stylesheet') ?>">
    <link href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet') ?>">
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div class="container-fluid">

  	 	<div class="header">
	     <div class="container">
	        <div class="row">
               <div class="col-md-2">
           
             </div>
	           <div class="col-md-8">
	              <!-- Logo -->
	              <div class="logo">
	                 <h3 align="center"><a href="index.html">Insurance Brokers System</a></h3 align="center">
	              </div>
	           </div>
	           <div class="col-md-2">
	         
	           </div>
	        </div>
	     </div>
	</div>

    <div class="page-content ">
    	<div class="row">
		  <div class="col-md-2" style="border-right:2px solid gray;height:800px; ">
		  	<div class="sidebar content-box" style="display: block; width:100%;">
                <ul class="nav">
                    <!-- Main menu -->
                    
                       <li class="current"><a href="<?php echo base_url('users/'); ?>"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                      <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Insuarance
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="<?php echo base_url('covernotes'); ?>">Cover Notes</a></li>
                            <li><a href="<?php echo base_url('policies'); ?>">Policies</a></li>
                        </ul>
                    </li>
                      <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Registration
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                              <li><a href="<?php echo base_url('beneficiaries'); ?>">Insuarers</a></li>
                              <li><a href="<?php echo base_url('covernotes_types'); ?>">Cover Notes Types</a></li>
                          
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url('reports'); ?>"><i class="glyphicon glyphicon-tasks"></i></i> Report</a></li>
                    <li><a href="<?php echo base_url('notify'); ?>"><i class="glyphicon glyphicon-list"></i> Notify</a></li>
                    <li><a href="<?php echo base_url('statistics'); ?>"><i class="glyphicon glyphicon-stats"></i> Statistics </a></li>
                    
                    <li><a href="<?php echo base_url('users'); ?>"><i class="glyphicon glyphicon-list"></i> Users</a></li>
                </ul>
             </div>
		  </div>
		  <div class="col-md-10">
		  
		  <?php $this->load->view($contents); ?>

		  </div>
		</div>
    </div>

    <footer>
         <div class="container-fluid">
         
            <div class="copy text-center">
               Copyright 2015<a href='http:/treadstoneconsult.com'target="_blank"> TREADSTONE CONSULT LTD</a>
            </div>
            
         </div>
      </footer>
  </div>
 

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/moment.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js') ?>"></script>
  </body>
</html>