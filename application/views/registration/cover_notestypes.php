<section>
<div class="col-md-12">
	<div class="content-box-large">
		<div class="panel-heading">
			<div class="panel-tit6"><h3  align="center">Please add Covernotes type</h3>
				<div style="border-bottom: solid black 2px;"></div>
			</div>
		</div>
		<div class="panel-body">
		<form class="form-horizontal" role="form"action="<?php echo base_url("addcovernotetype") ?>" method="post">
			
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-6">
					<?php echo $this->session->flashdata('message'); ?>
				</div>
			
			</div>
			<div class="form-group">
				<label  class="col-sm-3 control-label">Cover Note name</label>
				<div class="col-sm-6">
				<input type="text" class="form-control input input-sm"  name="covernote" value="<?php echo set_value('beneficiary')?>" placeholder="Beneficiary name">
				<?php echo form_error('covernote'); ?>
				</div>
			</div>
							
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-6">
						<button type="submit"  style="background:#ff7f2a;color:white; font-size:12px;" class="btn btn-sm btn-block">Save</button>
				</div>
			</div>
		</form>


		</div>
	</div>
</div>
</section>